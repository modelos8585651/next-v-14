import { getUserById} from "@/services/usuarios/retorna-id";

export default async function UserDetailsPage({params}:{params:{usuarioId:String}}){
    const user = await getUserById(params.usuarioId)
    return(
        <>
        <h2>Usuario: {user.first_name} {user.last_name}</h2>
        <span>{user.email}</span>
        </>
    )
}