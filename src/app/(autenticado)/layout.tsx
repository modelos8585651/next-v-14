import { Inter } from 'next/font/google'
import '../globals.css'
import Link from 'next/link'
import Head from './head'
import styles from "@/styles/sistema.module.css";
import MenuLateral from '@/components/dashboard/menulateral/menulateral';
import MenuTopo from '@/components/dashboard/menutopo/menutopo';
import Rodape from '@/components/dashboard/rodape/rodape';
const inter = Inter({ subsets: ['latin'] })


export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">      
    <Head/>
      <body className={inter.className}>
      <div className={styles.container}>
      <div className={styles.menu}>
        <MenuLateral/>
      </div>
      <div className={styles.content}>
        <MenuTopo/>
        {children}
        <Rodape/>
      </div>
    </div>
        </body>
    </html>
  )
}
