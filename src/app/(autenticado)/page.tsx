import Card from "@/components/dashboard/card/card";
import Chart from "@/components/dashboard/chart/chart";
import TabelaRank from "@/components/dashboard/tabelaRank/tabelaRank";
import styles from "@/styles/sistema.module.css";
export default function Home() {
   const cards = [
    {
      id: 1,
      title: "Total Users",
      number: 10.928,
      change: 12,
    },
    {
      id: 2,
      title: "Stock",
      number: 8.236,
      change: -2,
    },
    {
      id: 3,
      title: "Revenue",
      number: 6.642,
      change: 18,
    },
  ];
  return (
    <>
     <div className={styles.wrapper}>
      <div className={styles.main}>
        <div className={styles.cards}>
          {cards.map((item) => (
            <Card item={item} key={item.id} />
          ))}
        </div>
        <TabelaRank />
        <Chart />
        </div>
        </div>
    </>
  )
}
