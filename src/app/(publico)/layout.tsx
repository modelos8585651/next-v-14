import { Inter } from "next/font/google"
import Head from "./head"
import '../../app/globals.css'

export const metadata = {
  title: 'Next.js',
  description: 'Generated by Next.js',
}
const inter = Inter({ subsets: ['latin'] })
export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <Head/>
      <body className={inter.className}>        
        {children}
        </body>
    </html>
  )
}
