"use client"
import { useRouter } from "next/navigation"
import styles from "../../../styles/login.module.css";


export default function LoginPage(){
    const router:any=useRouter();
async function entrar(e:any){
  e.preventDefault()
  router.push('/')
}
    return (
        <div className={styles.container}>
        <div className={styles.form}>
        <h1>Login</h1>
        <input type="text" placeholder="Usuário" name="username" />
        <input type="password" placeholder="Senha" name="password" />
       
        <button onClick={(e:any)=>{entrar(e)}} type="submit">Entrar</button>
      </div>
      </div>
    )
}