import styles from "./rodape.module.css";

const Rodape = () => {
  return (
    <div className={styles.container}>
      <div className={styles.logo}>7i9 Soluções Tecnológicas</div>
      <div className={styles.text}>© Todos os direitos reservados.</div>
    </div>
  );
};

export default Rodape;